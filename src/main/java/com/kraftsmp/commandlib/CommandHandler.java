package com.kraftsmp.commandlib;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class CommandHandler {

    private Map<String, CommandBase> commands;
    private JavaPlugin plugin;

    public CommandHandler(JavaPlugin plugin) {
        this.commands = new HashMap<>();
        this.plugin = plugin;
    }

    public boolean addCommand(CommandBase cmd) {
        if (this.commands.get(cmd.getName()) != null) {
            return false;
        }
        this.commands.put(cmd.getName(), cmd);
        return true;
    }

    public boolean run(String cmd, CommandSender sender, String[] args) {
        return this.commands.get(cmd).run(sender, args, this.plugin);
    }

    public boolean run(CommandBase cmd, CommandSender sender, String[] args) {
        return cmd.run(sender, args, this.plugin);
    }
}