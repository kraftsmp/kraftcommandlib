package com.kraftsmp.commandlib;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class CommandBase {

    private String name;

    public CommandBase(String name) {
        this.name = name.toLowerCase();
    }

    public String getName() {
        return this.name;
    }

    public void init() { /* Don't do anything if not overridden */ };

    public abstract boolean run(CommandSender sender, String[] args, JavaPlugin plugin);
}